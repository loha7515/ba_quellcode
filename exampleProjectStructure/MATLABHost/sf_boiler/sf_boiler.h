/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: sf_boiler.h
 *
 * Code generated for Simulink model 'sf_boiler'.
 *
 * Model version                  : 1.87
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Thu Nov 19 10:10:50 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Custom Processor->Custom
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_sf_boiler_h_
#define RTW_HEADER_sf_boiler_h_
#include <math.h>
#include <string.h>
#ifndef sf_boiler_COMMON_INCLUDES_
# define sf_boiler_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* sf_boiler_COMMON_INCLUDES_ */

#include "sf_boiler_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T Integrator;                   /* '<S2>/Integrator' */
  real_T Gain3;                        /* '<S2>/Gain3' */
  int8_T LED;                          /* '<Root>/Bang-Bang Controller' */
  int8_T boiler;                       /* '<Root>/Bang-Bang Controller' */
} BlockIO_sf_boiler;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  struct {
    void *LoggedData[3];
  } Scope_PWORK;                       /* '<Root>/Scope' */

  int8_T color;                        /* '<Root>/Bang-Bang Controller' */
  uint8_T is_active_c7_sf_boiler;      /* '<Root>/Bang-Bang Controller' */
  uint8_T is_c7_sf_boiler;             /* '<Root>/Bang-Bang Controller' */
  uint8_T is_LEDs_on;                  /* '<Root>/Bang-Bang Controller' */
  uint8_T was_LEDs_on;                 /* '<Root>/Bang-Bang Controller' */
  uint8_T temporalCounter_i1;          /* '<Root>/Bang-Bang Controller' */
} D_Work_sf_boiler;

/* Continuous states (auto storage) */
typedef struct {
  real_T Integrator_CSTATE;            /* '<S2>/Integrator' */
} ContinuousStates_sf_boiler;

/* State derivatives (auto storage) */
typedef struct {
  real_T Integrator_CSTATE;            /* '<S2>/Integrator' */
} StateDerivatives_sf_boiler;

/* State disabled  */
typedef struct {
  boolean_T Integrator_CSTATE;         /* '<S2>/Integrator' */
} StateDisabled_sf_boiler;

#ifndef ODE3_INTG
#define ODE3_INTG

/* ODE3 Integration Data */
typedef struct {
  real_T *y;                           /* output */
  real_T *f[3];                        /* derivatives */
} ODE3_IntgData;

#endif

/* Parameters (auto storage) */
struct Parameters_sf_boiler_ {
  real_T coolingrate_Value;            /* Expression: -0.1
                                        * Referenced by: '<S2>/cooling rate'
                                        */
  real_T heatingrate_Value;            /* Expression: 1
                                        * Referenced by: '<S2>/heating rate'
                                        */
  real_T Integrator_IC;                /* Expression: 15
                                        * Referenced by: '<S2>/Integrator'
                                        */
  real_T sensor_Coefs[2];              /* Expression: [ 0.05     0.75 ]
                                        * Referenced by: '<S3>/sensor'
                                        */
  real_T scale_Gain;                   /* Expression: 256/5
                                        * Referenced by: '<S4>/scale'
                                        */
  real_T quantize_Interval;            /* Expression: 1
                                        * Referenced by: '<S4>/quantize'
                                        */
  real_T limit_UpperSat;               /* Expression: 255
                                        * Referenced by: '<S4>/limit'
                                        */
  real_T limit_LowerSat;               /* Expression: 0
                                        * Referenced by: '<S4>/limit'
                                        */
  real_T Gain3_Gain;                   /* Expression: 1/25
                                        * Referenced by: '<S2>/Gain3'
                                        */
  int8_T temperaturesetpoint_Value;    /* Computed Parameter: temperaturesetpoint_Value
                                        * Referenced by: '<Root>/temperature set point'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_sf_boiler {
  const char_T *errorStatus;
  RTWSolverInfo solverInfo;
  ContinuousStates_sf_boiler *contStates;
  int_T *periodicContStateIndices;
  real_T *periodicContStateRanges;
  real_T *derivs;
  boolean_T *contStateDisabled;
  boolean_T zCCacheNeedsReset;
  boolean_T derivCacheNeedsReset;
  boolean_T CTOutputIncnstWithState;
  real_T odeY[1];
  real_T odeF[3][1];
  ODE3_IntgData intgData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Block parameters (auto storage) */
extern Parameters_sf_boiler sf_boiler_P;

/* Block signals (auto storage) */
extern BlockIO_sf_boiler sf_boiler_B;

/* Continuous states (auto storage) */
extern ContinuousStates_sf_boiler sf_boiler_X;

/* Block states (auto storage) */
extern D_Work_sf_boiler sf_boiler_DWork;

/* Model entry point functions */
extern void sf_boiler_initialize(void);
extern void sf_boiler_step(void);
extern void sf_boiler_terminate(void);

/* Real-time Model object */
extern RT_MODEL_sf_boiler *const sf_boiler_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'sf_boiler'
 * '<S1>'   : 'sf_boiler/Bang-Bang Controller'
 * '<S2>'   : 'sf_boiler/Boiler Plant model'
 * '<S3>'   : 'sf_boiler/Boiler Plant model/digital thermometer'
 * '<S4>'   : 'sf_boiler/Boiler Plant model/digital thermometer/ADC'
 */
#endif                                 /* RTW_HEADER_sf_boiler_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
