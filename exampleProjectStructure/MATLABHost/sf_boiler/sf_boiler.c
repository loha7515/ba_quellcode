/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: sf_boiler.c
 *
 * Code generated for Simulink model 'sf_boiler'.
 *
 * Model version                  : 1.87
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Thu Nov 19 10:10:50 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Custom Processor->Custom
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "sf_boiler.h"
#include "sf_boiler_private.h"

/* Named constants for Chart: '<Root>/Bang-Bang Controller' */
#define sf_boiler_GREEN                ((int8_T)2)
#define sf_boiler_IN_HIGH              ((uint8_T)1U)
#define sf_boiler_IN_LEDs_off          ((uint8_T)1U)
#define sf_boiler_IN_LEDs_on           ((uint8_T)2U)
#define sf_boiler_IN_NORM              ((uint8_T)2U)
#define sf_boiler_IN_NO_ACTIVE_CHILD   ((uint8_T)0U)
#define sf_boiler_OFF                  ((int8_T)0)
#define sf_boiler_ON                   ((int8_T)1)
#define sf_boiler_RED                  ((int8_T)1)

/* Block signals (auto storage) */
BlockIO_sf_boiler sf_boiler_B;

/* Continuous states */
ContinuousStates_sf_boiler sf_boiler_X;

/* Block states (auto storage) */
D_Work_sf_boiler sf_boiler_DWork;

/* Real-time model */
RT_MODEL_sf_boiler sf_boiler_M_;
RT_MODEL_sf_boiler *const sf_boiler_M = &sf_boiler_M_;

/* Forward declaration for local functions */
static void sf_boiler_flash_LED(void);
static boolean_T sf_boiler_cold(const int8_T *fixPttempdegC);
static boolean_T sf_boiler_warm(const int8_T *fixPttempdegC);
static void sf_boiler_turn_LED(int8_T mode);

/*
 * This function updates continuous states using the ODE3 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE3_A[3] = {
    1.0/2.0, 3.0/4.0, 1.0
  };

  static const real_T rt_ODE3_B[3][3] = {
    { 1.0/2.0, 0.0, 0.0 },

    { 0.0, 3.0/4.0, 0.0 },

    { 2.0/9.0, 1.0/3.0, 4.0/9.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE3_IntgData *id = (ODE3_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T hB[3];
  int_T i;
  int_T nXc = 1;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  sf_boiler_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE3_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[0]);
  rtsiSetdX(si, f1);
  sf_boiler_step();
  sf_boiler_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE3_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[1]);
  rtsiSetdX(si, f2);
  sf_boiler_step();
  sf_boiler_derivatives();

  /* tnew = t + hA(3);
     ynew = y + f*hB(:,3); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE3_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, tnew);
  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

real_T rt_roundd(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/* Function for Chart: '<Root>/Bang-Bang Controller' */
static void sf_boiler_flash_LED(void)
{
  /* MATLAB Function 'flash_LED': '<S1>:4' */
  /* Graphical Function 'flash_LED': '<S1>:4' */
  /* '<S1>:21:1' */
  if (sf_boiler_B.LED == sf_boiler_OFF) {
    /* '<S1>:29:1' */
    sf_boiler_B.LED = sf_boiler_DWork.color;
  } else {
    /* '<S1>:13:1' */
    sf_boiler_B.LED = sf_boiler_OFF;
  }
}

/* Function for Chart: '<Root>/Bang-Bang Controller' */
static boolean_T sf_boiler_cold(const int8_T *fixPttempdegC)
{
  /* Constant: '<Root>/temperature set point' */
  /* MATLAB Function 'cold': '<S1>:6' */
  /* Graphical Function 'cold': '<S1>:6' */
  /* '<S1>:22:1' */
  return *fixPttempdegC * 25 <= (sf_boiler_P.temperaturesetpoint_Value << 6);
}

/* Function for Chart: '<Root>/Bang-Bang Controller' */
static boolean_T sf_boiler_warm(const int8_T *fixPttempdegC)
{
  boolean_T b;

  /* MATLAB Function 'warm': '<S1>:9' */
  /* Graphical Function 'warm': '<S1>:9' */
  /* '<S1>:28:1' */
  b = !sf_boiler_cold(fixPttempdegC);
  return b;
}

/* Function for Chart: '<Root>/Bang-Bang Controller' */
static void sf_boiler_turn_LED(int8_T mode)
{
  /* MATLAB Function 'turn_LED': '<S1>:5' */
  /* Graphical Function 'turn_LED': '<S1>:5' */
  /* '<S1>:15:1' */
  if (mode == sf_boiler_ON) {
    /* '<S1>:17:1' */
    sf_boiler_DWork.color = sf_boiler_GREEN;
  } else {
    /* '<S1>:18:1' */
    sf_boiler_DWork.color = sf_boiler_RED;
  }

  /* '<S1>:19:1' */
  sf_boiler_B.LED = sf_boiler_DWork.color;

  /* '<S1>:19:1' */
  sf_boiler_B.boiler = mode;
}

/* Model step function */
void sf_boiler_step(void)
{
  boolean_T sf_internal_predicateOutput;
  real_T rtb_limit;
  int8_T fixPttempdegC;
  if (rtmIsMajorTimeStep(sf_boiler_M)) {
    /* set solver stop time */
    rtsiSetSolverStopTime(&sf_boiler_M->solverInfo,
                          ((sf_boiler_M->Timing.clockTick0+1)*
      sf_boiler_M->Timing.stepSize0));
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(sf_boiler_M)) {
    sf_boiler_M->Timing.t[0] = rtsiGetT(&sf_boiler_M->solverInfo);
  }

  /* Integrator: '<S2>/Integrator' */
  sf_boiler_B.Integrator = sf_boiler_X.Integrator_CSTATE;

  /* Quantizer: '<S4>/quantize' incorporates:
   *  Gain: '<S4>/scale'
   *  Polyval: '<S3>/sensor'
   */
  rtb_limit = rt_roundd((sf_boiler_P.sensor_Coefs[0] * sf_boiler_B.Integrator +
    sf_boiler_P.sensor_Coefs[1]) * sf_boiler_P.scale_Gain /
                        sf_boiler_P.quantize_Interval) *
    sf_boiler_P.quantize_Interval;

  /* Saturate: '<S4>/limit' */
  if (rtb_limit > sf_boiler_P.limit_UpperSat) {
    rtb_limit = sf_boiler_P.limit_UpperSat;
  } else {
    if (rtb_limit < sf_boiler_P.limit_LowerSat) {
      rtb_limit = sf_boiler_P.limit_LowerSat;
    }
  }

  /* End of Saturate: '<S4>/limit' */

  /* DataTypeConversion: '<S3>/Linear fixed point conversion' */
  rtb_limit = fmod(floor(rtb_limit), 256.0);
  fixPttempdegC = (int8_T)(rtb_limit < 0.0 ? (int32_T)(int8_T)-(int8_T)(uint8_T)
    -rtb_limit : (int32_T)(int8_T)(uint8_T)rtb_limit);
  if (rtmIsMajorTimeStep(sf_boiler_M)) {
    /* Chart: '<Root>/Bang-Bang Controller' */
    if (sf_boiler_DWork.temporalCounter_i1 < 63U) {
      sf_boiler_DWork.temporalCounter_i1++;
    }

    /* Gateway: Bang-Bang
       Controller */
    /* During: Bang-Bang
       Controller */
    if (sf_boiler_DWork.is_active_c7_sf_boiler == 0U) {
      /* Entry: Bang-Bang
         Controller */
      sf_boiler_DWork.is_active_c7_sf_boiler = 1U;

      /* Entry Internal: Bang-Bang
         Controller */
      /* Transition: '<S1>:10' */
      sf_boiler_DWork.is_c7_sf_boiler = sf_boiler_IN_LEDs_off;
      sf_boiler_DWork.temporalCounter_i1 = 0U;

      /* Entry 'LEDs_off': '<S1>:2' */
      sf_boiler_turn_LED(sf_boiler_OFF);
    } else if (sf_boiler_DWork.is_c7_sf_boiler == sf_boiler_IN_LEDs_off) {
      /* During 'LEDs_off': '<S1>:2' */
      if ((sf_boiler_DWork.temporalCounter_i1 >= 40U) && sf_boiler_cold
          (&fixPttempdegC)) {
        sf_internal_predicateOutput = true;
      } else {
        sf_internal_predicateOutput = false;
      }

      if (sf_internal_predicateOutput) {
        /* Transition: '<S1>:12' */
        sf_boiler_DWork.is_c7_sf_boiler = sf_boiler_IN_LEDs_on;
        sf_boiler_DWork.temporalCounter_i1 = 0U;

        /* Entry 'LEDs_on': '<S1>:3' */
        sf_boiler_turn_LED(sf_boiler_ON);

        /* Entry Internal 'LEDs_on': '<S1>:3' */
        switch (sf_boiler_DWork.was_LEDs_on) {
         case sf_boiler_IN_HIGH:
          sf_boiler_DWork.is_LEDs_on = sf_boiler_IN_HIGH;
          sf_boiler_DWork.was_LEDs_on = sf_boiler_IN_HIGH;
          break;

         case sf_boiler_IN_NORM:
          sf_boiler_DWork.is_LEDs_on = sf_boiler_IN_NORM;
          sf_boiler_DWork.was_LEDs_on = sf_boiler_IN_NORM;
          break;

         default:
          /* Transition: '<S1>:23' */
          sf_boiler_DWork.is_LEDs_on = sf_boiler_IN_HIGH;
          sf_boiler_DWork.was_LEDs_on = sf_boiler_IN_HIGH;
          break;
        }
      }
    } else {
      /* During 'LEDs_on': '<S1>:3' */
      if (sf_boiler_DWork.temporalCounter_i1 >= 20U) {
        /* Transition: '<S1>:11' */
        /* Exit Internal 'LEDs_on': '<S1>:3' */
        sf_boiler_DWork.is_LEDs_on = sf_boiler_IN_NO_ACTIVE_CHILD;
        sf_boiler_DWork.is_c7_sf_boiler = sf_boiler_IN_LEDs_off;
        sf_boiler_DWork.temporalCounter_i1 = 0U;

        /* Entry 'LEDs_off': '<S1>:2' */
        sf_boiler_turn_LED(sf_boiler_OFF);
      } else {
        sf_boiler_flash_LED();
        if (sf_boiler_DWork.is_LEDs_on == sf_boiler_IN_HIGH) {
          /* During 'HIGH': '<S1>:7' */
          if (sf_boiler_warm(&fixPttempdegC)) {
            /* Transition: '<S1>:24' */
            sf_boiler_DWork.is_LEDs_on = sf_boiler_IN_NORM;
            sf_boiler_DWork.was_LEDs_on = sf_boiler_IN_NORM;
          }
        } else {
          /* During 'NORM': '<S1>:8' */
          if (sf_boiler_warm(&fixPttempdegC)) {
            /* Transition: '<S1>:25' */
            sf_boiler_DWork.is_LEDs_on = sf_boiler_IN_NO_ACTIVE_CHILD;
            sf_boiler_DWork.is_c7_sf_boiler = sf_boiler_IN_LEDs_off;
            sf_boiler_DWork.temporalCounter_i1 = 0U;

            /* Entry 'LEDs_off': '<S1>:2' */
            sf_boiler_turn_LED(sf_boiler_OFF);
          }
        }
      }
    }

    /* End of Chart: '<Root>/Bang-Bang Controller' */
    /* Switch: '<S2>/Switch' incorporates:
     *  Constant: '<S2>/cooling rate'
     *  Constant: '<S2>/heating rate'
     *  DataTypeConversion: '<S2>/Data Type  Conversion1'
     */
    if (sf_boiler_B.boiler != 0) {
      rtb_limit = sf_boiler_P.heatingrate_Value;
    } else {
      rtb_limit = sf_boiler_P.coolingrate_Value;
    }

    /* End of Switch: '<S2>/Switch' */

    /* Gain: '<S2>/Gain3' */
    sf_boiler_B.Gain3 = sf_boiler_P.Gain3_Gain * rtb_limit;
  }

  if (rtmIsMajorTimeStep(sf_boiler_M)) {
    rt_ertODEUpdateContinuousStates(&sf_boiler_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     */
    ++sf_boiler_M->Timing.clockTick0;
    sf_boiler_M->Timing.t[0] = rtsiGetSolverStopTime(&sf_boiler_M->solverInfo);

    {
      /* Update absolute timer for sample time: [1.0s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 1.0, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       */
      sf_boiler_M->Timing.clockTick1++;
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void sf_boiler_derivatives(void)
{
  StateDerivatives_sf_boiler *_rtXdot;
  _rtXdot = ((StateDerivatives_sf_boiler *) sf_boiler_M->derivs);

  /* Derivatives for Integrator: '<S2>/Integrator' */
  _rtXdot->Integrator_CSTATE = sf_boiler_B.Gain3;
}

/* Model initialize function */
void sf_boiler_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)sf_boiler_M, 0,
                sizeof(RT_MODEL_sf_boiler));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&sf_boiler_M->solverInfo,
                          &sf_boiler_M->Timing.simTimeStep);
    rtsiSetTPtr(&sf_boiler_M->solverInfo, &rtmGetTPtr(sf_boiler_M));
    rtsiSetStepSizePtr(&sf_boiler_M->solverInfo, &sf_boiler_M->Timing.stepSize0);
    rtsiSetdXPtr(&sf_boiler_M->solverInfo, &sf_boiler_M->derivs);
    rtsiSetContStatesPtr(&sf_boiler_M->solverInfo, (real_T **)
                         &sf_boiler_M->contStates);
    rtsiSetNumContStatesPtr(&sf_boiler_M->solverInfo,
      &sf_boiler_M->Sizes.numContStates);
    rtsiSetNumPeriodicContStatesPtr(&sf_boiler_M->solverInfo,
      &sf_boiler_M->Sizes.numPeriodicContStates);
    rtsiSetPeriodicContStateIndicesPtr(&sf_boiler_M->solverInfo,
      &sf_boiler_M->periodicContStateIndices);
    rtsiSetPeriodicContStateRangesPtr(&sf_boiler_M->solverInfo,
      &sf_boiler_M->periodicContStateRanges);
    rtsiSetErrorStatusPtr(&sf_boiler_M->solverInfo, (&rtmGetErrorStatus
      (sf_boiler_M)));
    rtsiSetRTModelPtr(&sf_boiler_M->solverInfo, sf_boiler_M);
  }

  rtsiSetSimTimeStep(&sf_boiler_M->solverInfo, MAJOR_TIME_STEP);
  sf_boiler_M->intgData.y = sf_boiler_M->odeY;
  sf_boiler_M->intgData.f[0] = sf_boiler_M->odeF[0];
  sf_boiler_M->intgData.f[1] = sf_boiler_M->odeF[1];
  sf_boiler_M->intgData.f[2] = sf_boiler_M->odeF[2];
  sf_boiler_M->contStates = ((ContinuousStates_sf_boiler *) &sf_boiler_X);
  rtsiSetSolverData(&sf_boiler_M->solverInfo, (void *)&sf_boiler_M->intgData);
  rtsiSetSolverName(&sf_boiler_M->solverInfo,"ode3");
  rtmSetTPtr(sf_boiler_M, &sf_boiler_M->Timing.tArray[0]);
  sf_boiler_M->Timing.stepSize0 = 1.0;

  /* block I/O */
  (void) memset(((void *) &sf_boiler_B), 0,
                sizeof(BlockIO_sf_boiler));

  {
    sf_boiler_B.Integrator = 0.0;
    sf_boiler_B.Gain3 = 0.0;
  }

  /* states (continuous) */
  {
    (void) memset((void *)&sf_boiler_X, 0,
                  sizeof(ContinuousStates_sf_boiler));
  }

  /* states (dwork) */
  (void) memset((void *)&sf_boiler_DWork, 0,
                sizeof(D_Work_sf_boiler));

  /* InitializeConditions for Integrator: '<S2>/Integrator' */
  sf_boiler_X.Integrator_CSTATE = sf_boiler_P.Integrator_IC;

  /* SystemInitialize for Chart: '<Root>/Bang-Bang Controller' */
  sf_boiler_DWork.is_LEDs_on = sf_boiler_IN_NO_ACTIVE_CHILD;
  sf_boiler_DWork.was_LEDs_on = sf_boiler_IN_NO_ACTIVE_CHILD;
  sf_boiler_DWork.temporalCounter_i1 = 0U;
  sf_boiler_DWork.is_active_c7_sf_boiler = 0U;
  sf_boiler_DWork.is_c7_sf_boiler = sf_boiler_IN_NO_ACTIVE_CHILD;
}

/* Model terminate function */
void sf_boiler_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
