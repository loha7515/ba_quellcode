/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: sf_boiler_types.h
 *
 * Code generated for Simulink model 'sf_boiler'.
 *
 * Model version                  : 1.87
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Thu Nov 19 10:10:50 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Custom Processor->Custom
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_sf_boiler_types_h_
#define RTW_HEADER_sf_boiler_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_sf_boiler_ Parameters_sf_boiler;

/* Forward declaration for rtModel */
typedef struct tag_RTM_sf_boiler RT_MODEL_sf_boiler;

#endif                                 /* RTW_HEADER_sf_boiler_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
