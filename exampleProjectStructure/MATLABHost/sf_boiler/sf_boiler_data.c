/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: sf_boiler_data.c
 *
 * Code generated for Simulink model 'sf_boiler'.
 *
 * Model version                  : 1.87
 * Simulink Coder version         : 8.13 (R2017b) 24-Jul-2017
 * C/C++ source code generated on : Thu Nov 19 10:10:50 2020
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Custom Processor->Custom
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "sf_boiler.h"
#include "sf_boiler_private.h"

/* Block parameters (auto storage) */
Parameters_sf_boiler sf_boiler_P = {
  /* Expression: -0.1
   * Referenced by: '<S2>/cooling rate'
   */
  -0.1,

  /* Expression: 1
   * Referenced by: '<S2>/heating rate'
   */
  1.0,

  /* Expression: 15
   * Referenced by: '<S2>/Integrator'
   */
  15.0,

  /* Expression: [ 0.05     0.75 ]
   * Referenced by: '<S3>/sensor'
   */
  { 0.05, 0.75 },

  /* Expression: 256/5
   * Referenced by: '<S4>/scale'
   */
  51.2,

  /* Expression: 1
   * Referenced by: '<S4>/quantize'
   */
  1.0,

  /* Expression: 255
   * Referenced by: '<S4>/limit'
   */
  255.0,

  /* Expression: 0
   * Referenced by: '<S4>/limit'
   */
  0.0,

  /* Expression: 1/25
   * Referenced by: '<S2>/Gain3'
   */
  0.04,

  /* Computed Parameter: temperaturesetpoint_Value
   * Referenced by: '<Root>/temperature set point'
   */
  35
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
