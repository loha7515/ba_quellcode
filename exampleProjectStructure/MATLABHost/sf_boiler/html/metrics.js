function CodeMetrics() {
	 this.metricsArray = {};
	 this.metricsArray.var = new Array();
	 this.metricsArray.fcn = new Array();
	 this.metricsArray.var["rtDW"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	size: 17};
	 this.metricsArray.var["rtM_"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	size: 489};
	 this.metricsArray.var["rtX"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	size: 8};
	 this.metricsArray.fcn["ceil"] = {file: "/opt/Mathworks/2017/sys/lcc/include/math.h",
	stack: 0,
	stackTotal: 0};
	 this.metricsArray.fcn["fabs"] = {file: "/opt/Mathworks/2017/sys/lcc/include/math.h",
	stack: 0,
	stackTotal: 0};
	 this.metricsArray.fcn["floor"] = {file: "/opt/Mathworks/2017/sys/lcc/include/math.h",
	stack: 0,
	stackTotal: 0};
	 this.metricsArray.fcn["memcpy"] = {file: "/opt/Mathworks/2017/sys/lcc/include/string.h",
	stack: 0,
	stackTotal: 0};
	 this.metricsArray.fcn["rt_roundd_snf"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	stack: 8,
	stackTotal: 8};
	 this.metricsArray.fcn["sf_boiler.c:cold"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	stack: 0,
	stackTotal: 0};
	 this.metricsArray.fcn["sf_boiler.c:flash_LED"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	stack: 0,
	stackTotal: 0};
	 this.metricsArray.fcn["sf_boiler.c:rt_ertODEUpdateContinuousStates"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	stack: 104,
	stackTotal: -1};
	 this.metricsArray.fcn["sf_boiler.c:turn_boiler"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	stack: 0,
	stackTotal: 0};
	 this.metricsArray.fcn["sf_boiler.c:warm"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	stack: 1,
	stackTotal: 1};
	 this.metricsArray.fcn["sf_boiler_derivatives"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	stack: 8,
	stackTotal: 8};
	 this.metricsArray.fcn["sf_boiler_initialize"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	stack: 0,
	stackTotal: 0};
	 this.metricsArray.fcn["sf_boiler_step"] = {file: "/home/strawberry-user/sf_boiler_ert_rtw/sf_boiler.c",
	stack: 10,
	stackTotal: -1};
	 this.getMetrics = function(token) { 
		 var data;
		 data = this.metricsArray.var[token];
		 if (!data) {
			 data = this.metricsArray.fcn[token];
			 if (data) data.type = "fcn";
		 } else { 
			 data.type = "var";
		 }
	 return data; }; 
	 this.codeMetricsSummary = '<a href="sf_boiler_metrics.html">Global Memory: 514(bytes) Maximum Stack: 104(bytes)</a>';
	}
CodeMetrics.instance = new CodeMetrics();
