#!/bin/bash
. ./userSpecific.config

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$BASH_SOURCE")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
export LLDBCONFIGPATH=$SCRIPTPATH
echo "SCRIPTPATH: $SCRIPTPATH"

if [ -z "$2" ]
then
echo "Please provide relative path to source file as second argument"
exit
else
export MADSRCPATH="$SCRIPTPATH/$2"
echo "MADSRCPATH: $MADSRCPATH"
fi

if [ -z "$1" ]
then
echo "Please provide executable to be debugged as first argument"
else
EXECUTABLE=$1
EXECUTABLEPATH=$(dirname "$EXECUTABLE")
cd $EXECUTABLEPATH
eval 'lldb $SCRIPTPATH/$EXECUTABLE'

fi
