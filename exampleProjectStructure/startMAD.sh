#!/bin/bash 

#TODO source Python Environment - or just let i to the user?

# make sure to use the correct gdbgui module, not the one from pip
. userSpecific.config
echo "GDBGUIMODULEPATH: $GDBGUIMODULEPATH"
#TODO: Make sure Configuration for Plugin is available - i think that is ready, but has to be doublechecked again

#TODO return with usage-advice when no target is specified as $1

# TODO : Find a way to tell react, where the plugin resides, currenty, it just assumes the Homedir


# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$BASH_SOURCE")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
export LLDBCONFIGPATH=$SCRIPTPATH
echo "SCRIPTPATH: $SCRIPTPATH"

if [ -z "$2" ]
then
echo "Please provide relative path to source file as second argument"
exit
else
export MADSRCPATH="$SCRIPTPATH/$2"
echo "MADSRCPATH: $MADSRCPATH"
fi


EXECUTABLE=$1
EXECUTABLEPATH=$(dirname "$EXECUTABLE")
export PYTHONPATH="$GDBGUIMODULEPATH"
cd $EXECUTABLEPATH

python -m gdbgui -g lldb-mi --debug "$SCRIPTPATH/$EXECUTABLE"
