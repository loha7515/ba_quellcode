#!/bin/bash

########################################################################################
# Copyright (c) 2017-2018  OFFIS e.V., Oldenburg, Germany
# All rights reserved.
#
# ------------------------------------------------------------------------
# Pre-Source Code Transformation Script for the Model Aware Debugging
# ------------------------------------------------------------------------
# This Script is part of the Model Aware Debugging Code Base. 
# Copyright (c) 2018 OFFIS e.V. Institute for Information Technology
#
# Running this script with a source file as input argument will search the source file for
# #define macros of Simulink/Stateflow states and replace them with static const variables. 
# The reason for this is that, #defines are not cpatured in the CLANG AST so creating the 
# debug information (Model <-> Code association /Tags) needed by the debugger to later debug 
# the model is not possible with miminmal effort. Static const variables on the contrary are
# captured in CLANG's AST making easier to parse the AST and retrieve relevant information
# which can be stored as debug information 
#
# For example :
#    #define IN_DiscPresent  ((uint8_T)1U) --> static const uint8_T IN_DiscPresent = ((uint8_T)1U)
#
# OFFIS e.V.
# Institute for Information Technology
#   Escherweg 2
#   D-26121 Oldenburg
#   Germany
# www  : http://offis.de
# phone: +49 (441) 9722-0
# fax  : +49 (441) 9722-102
# ------------------------------------------------------------------------
# Author(s): Bewoayia Kebianyor
# ------------------------------------------------------------------------


echo "STARTING Pre-Source Code Transformation Script for the Model Aware Debugging "

if [ -z "$1" ]; then
 
    echo "No argument supplied."
	echo "Usage ./preSourceTransformation.sh source_file"
    exit
fi

TEMP_SRC_FILE="$1.tmp"
echo 
echo "Creating Back-Up for source file ...."
BACKUP_SRC_FILE="$1.bkup"

cp $1 $BACKUP_SRC_FILE

if [ ! -f $BACKUP_SRC_FILE ]; then
	echo "Failed to backup file"
else
	echo "Backup file sucessfully created !!"
fi

# Modify the file
echo "Start Translating file ..."
CHANGE_LOG_FILE="changeLog.txt"
#sed -s '/IN_/s/#define/static const uint8_T/igw "changelog.txt; ' "$1" > "$TEMP_SRC_FILE"
sed -s '/IN_/s/#define/static const uint8_T/igw change_log' "$1" > "$TEMP_SRC_FILE"

if [ -s change_log ]; then
    echo "File translated"
    sed -i '/static const uint8_T/s/.((uint8_T)*/=&/ig; ' "$TEMP_SRC_FILE"
	sed -i '/static const uint8_T/s/.*/&;/ig; ' "$TEMP_SRC_FILE"
else
    echo "No translation required. File already in acceptable format"
fi
cp $TEMP_SRC_FILE $1
rm $TEMP_SRC_FILE
rm -rf change_log

echo "Pre-Source Code Transformation Sucessfully Completed !!."





