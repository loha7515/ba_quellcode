/**
 * A component to render source code, assembly, and break points
 */

import { store } from "statorgfc";
import React from "react";
import SVG from "react-inlinesvg";
import mySvg from "../../../tempfiles/blockdiagram.svg";
import React from 'react';
import GdbApi from './GdbApi.jsx';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";


class ModelDiag extends React.Component {

  constructor(props) {
    super(props);
    this.states = ["MLLLL"];
    this.states1=["MCLCLCLCL"];
    this.loadDivElemente = this.loadDivElemente.bind(this);
    this.setBreakpoint_on_modell = this.setBreakpoint_on_modell.bind(this);
    this.removeBreakpoint_on_modell= this.removeBreakpoint_on_modell.bind(this);
    this.loadXmlDatei = this.loadXmlDatei.bind(this);
    this.ParseParentElement_stateflow_Datei = this.ParseParentElement_stateflow_Datei.bind(this);
    this.ParseParentElement_Simulink_Datei = this.ParseParentElement_Simulink_Datei.bind(this);
    this.vergleicheXMLSVGgeparsteElemente= this.vergleicheXMLSVGgeparsteElemente.bind(this);
    window.breakpointId = 0;
    this.resize = true;
    this.handle_bkpts_from_console = this.handle_bkpts_from_console.bind(this);
    this.svgParser=this.svgParser.bind(this);  
    this.getStates=0; 
  }
  componentDidMount() {
    setInterval(() => this.handle_bkpts_from_console());
    setInterval(() => this.loadDivElemente());
    setInterval(() => this.svgParser());   
  }
/*Der XML DAtei beinhaltet mehrere Objekten und daraus sollten die Name der Zustände geparst und gespeichert werden. 
  eine Recursive Parse der Datei findet hier statt
  Hier den Parser wird gewählt je nach der Art der angegebenen Datei. 
  Alle Stateflow Datei haben als erster Parentelement: machine und unterscheidet sich somit von simulink Datei*/
loadXmlDatei= function(){           
var data = require('../../../tempfiles/system_root_block.xml'); 

  return data;
}
// finde das Parentelement, Simulink Blöcke beinhaltet sind
FindeParentElement_Simulink_Datei= function (data) { 
    var firstParse = Object.values(data)[0];
    var secondParse = Object.values(firstParse)[1];
  
return secondParse;             
}
// finde das Parentelement, Stateflow Blöcke beinhaltet sind
FindeParentElement_stateflow_Datei= function (data) { 
    var firstParse = Object.values(data)[0];
    var datalen= Object.keys(firstParse).length;
    var secondParse = Object.values(firstParse);     
    var thirdParse = Object.values(secondParse)[datalen-1];
    var fourthParse = Object.values(thirdParse)[0];   

return fourthParse;
}
// Parse das gefundene Parentelement für Stateflow und bekommt ein Array von Statesnamen
ParseParentElement_stateflow_Datei= function (path) {
    var i=0;
    var tenParse = [];
    var neuState=[];
    var children = []; 
    var labelstring=[];
    var tempArray =[];
    
    for (var s = 0; s < Object.keys(Object.values(path)[0]).length; s++) {
     tenParse.push(Object.values(Object.values(path)[0])[s]);
      
    }
    
    // prüfe der erste State Element, speiche die erste Labelstring der XML Datei im Array
    // falls er ein Kinder hat, dann speiche ihn in children
    for (var s = 0; s < Object.values(tenParse).length; s++) {
      var child = Object.values(Object.values(tenParse)[s]).length;
      
       labelstring.push(Object.values(Object.values(Object.values(tenParse)[s])[1][0])[0]);
       

      // falls die gefundene State children haben dann parse sie 
      
      if (Object.keys(Object.values(Object.values(tenParse)[s])[child - 1][0])[0] === "state") {
        children = Object.values(Object.values(tenParse)[s])[child - 1][0];
        for (var l = 0; l < Object.keys(children).length; l++) {
          if (Object.keys(children)[l] === "state") {
            for (var m = 0; m < Object.values(children)[l].length; m++) {
              var child = Object.values(Object.values(children)[l][m]).length;

               labelstring.push(Object.values(Object.values(Object.values(children)[l][m])[1][0])[0]);
              
              for (var i = 0; i < neuState.length; i++) { 
                if (neuState[i] === "state") {
                  tempArray.push(Object.values(Object.values(children)[l][m])[child - 1][0]);
                  this.ParseParentElement(tempArray);
                }
              }
            }
          }
        }
      }
    }
// gefundene Statenamen sollen in Labelstring gespeichert werden    
return labelstring;


}
// Parse das gefundene Parentelement für simulink und bekommt ein Array von Statesnamen
ParseParentElement_Simulink_Datei= function (path) {
var labelstring=[];
// parse all State in XML Datei ( der Parser von Simulink-Dateien)
  for( var s=0; s<path.length ; s++){
     labelstring.push(Object.values(Object.values(Object.values(path))[s])[0].Name);
  } 
// gefundene Statenamen sollen in Labelstring gespeichert werden    
   return labelstring; 

}
svgParser=function(pathElement){
      var i=0;
      var k=0;
      var j=0;
      var states=[];
      var arr=[];
      let options=[["Stateflow","MCLCLCLCL"],["Simulink","MLLLL"]];
      let state_path= new Map(options);
      var svgElemente=[];
      var haschmap= new Map();
      for (i = 1; i < document.getElementsByTagName("g").length; i++) {
        if (document.getElementsByTagName("g")[i].innerHTML.replace(new RegExp("[^MLC]","g"),"")==state_path.get(pathElement) &&
            document.getElementsByTagName("g")[i].getAttribute("stroke")=="#000000" && 
            document.getElementsByTagName("g")[i].innerHTML.split(" ")[1].split(",")[0]=='d="M0') {
            arr.push(document.getElementsByTagName("g")[i].getBoundingClientRect());
            for ( k=0; k<i; k++){
             if(document.getElementsByTagName("g")[i+k].innerHTML.split(" ")[0]==="<text"){
                states.push(document.getElementsByTagName("g")[i+k].innerHTML.split(">"));
                break;
               }
              
              }
           
        }
      } 
      // finde die Statesnamen in svg
      var st=0;
      for(i in states){
       for( j in states[i]){
         if(states[i][j].split("</text").length===2){
         
           if(st===0){
             st= states[i][j].split("</text")[0]+ "\ ";
           }else{
            st= st + states[i][j].split("</text")[0]+ "\ ";
           }
         } 
      
        } 
       svgElemente.push(st.trim());
       st=0;
      }
     // speiche die relativen POsition der ZUstände undderen NAmen in MAp
     for(var i in arr,svgElemente){
          haschmap.set(arr[i],svgElemente[i]);
     }
return haschmap;
}

vergleicheXMLSVGgeparsteElemente= function() {
  var hilfsarray =[];
  var statesName1 = new Map;
  var xmlNamen=[];
  var i=0;
  var parentelm = this.FindeParentElement_Simulink_Datei(this.loadXmlDatei());
  // speiche das Ergebnis des Parsers( Name der States / Blöcke)
  hilfsarray = this.ParseParentElement_Simulink_Datei(parentelm); 
  var svgNamen= this.svgParser(this.modellTyp());    
    for( i=0; i<hilfsarray.length;i++){
        if(hilfsarray[i].includes("\n")){
          xmlNamen.push(hilfsarray[i].split("\n")[0].toLowerCase());
        }else { 
          xmlNamen.push(hilfsarray[i].toLowerCase());
        }
    } 

    for( var i of svgNamen.keys()){ 
     for(var item in xmlNamen){    
        if(svgNamen.get(i).toLowerCase().trim().includes(xmlNamen[item])){
          statesName1.set(svgNamen.get(i),i);
        }
     }
  }
  
  return statesName1;    
}
 
loadDivElemente() {
    var i = 0;
    let div_Container=0;
    var statesName1= this.vergleicheXMLSVGgeparsteElemente(); 
    var div_state=[]
 
// baut auf jede ZUstände ein unsichbares div, damit das setzen von Breakpoint visualisiert wird.

  for (var n of statesName1.keys()) { 

    div_Container = document.createElement('div');
    document.getElementById('model_diag_view').appendChild(div_Container);     
    div_Container.id = n;     
    div_Container.setAttribute("style", "y:" + (statesName1.get(n).y) + "px; border-radius:6px;  x:" + (statesName1.get(n).x) + "px; rigth:" + (statesName1.get(n).right) + "px; left:" + (statesName1.get(n).left) + "px; bottom:" + (statesName1.get(n).bottom) + "px; width:" + (statesName1.get(n).width) + "px; height:" + (statesName1.get(n).height) + "px;top:" + (statesName1.get(n).top) + "px;position:absolute ;float:inherit;visibility: visible;");
    
    div_state.push(div_Container);            
  }
  div_state.forEach(function (elem) {
    $(elem).bind("contextmenu", function (event) {
      window.breakpointId = elem.id;

      event.preventDefault();
      $(".context")
        .show()
        .css({
          top: event.pageY + 15,
          left: event.pageX + 10,
          
        });
    });
  });
 
return div_state;
}

// Contextmenu wird erstellt nach einem linken KLick auf den unsichbaren div(an der Stelle wo Zustände sich befindet)

//der Div wird Blau zur Benachrichtigung, das ein Breakpoint gesetzt wurde  
setBreakpoint_on_modell() {
  var div_states =this.loadDivElemente();
 div_states.forEach(function (elem) {
    if (window.breakpointId === elem.id) {
     GdbApi.model_breakpoint_set_on_state(elem.id);
    }
   $(".context").fadeOut("fast");
 });
}


removeBreakpoint_on_modell(){
 // holt alle gebauten Div_Container 
var div_states =this.loadDivElemente();
 div_states.forEach(function (elem) {
   if (window.breakpointId === elem.id){
         GdbApi.model_breakpoint_remove_Breakpoint_on_state(elem.id);
              
   }
    $(".context").fadeOut("fast");
  });
}
handle_bkpts_from_console() {            
 var i=0;
 // holt alle gebauten Div_Container 
 var div_states = this.loadDivElemente();

  try {
    div_states.forEach(function (elem) {
      if(GdbApi.returnStateId().includes(elem.id)||GdbApi.returnStateId()===elem.id|| 
        GdbApi.returnStateId().replace(/\\/g, "").includes(elem.id)){
         
          var stateId = GdbApi.Test()
        for (var item in stateId){    
         
          if (stateId[item].message ==="breakpoint-created") {           

            document.getElementById(elem.id).style.border="4px solid #337ab7";   
             stateId[item].message=null;            
            break;
          } 
          if (stateId[item].type ==="output" && stateId[item].payload.includes("Breakpoint line") ) {      
            document.getElementById(elem.id).style.border="4px solid transparent"; 
               
          }  
        }       
       }
    
    });
  }
  catch(err) {    
    var error=[];
    error.push(err.message);
  }
  

}
modellTyp(){
  
 try{

  const btn = document.querySelector('#btn');
  const sb = document.querySelector('#modell')
  btn.onclick = (event) => {
  
    event.preventDefault();
    this.getStates = $( "#modell option:selected" ).val(); 

    if(this.getStates!=""){
      document.getElementById("model").style.visibility="visible";
      document.getElementById("selected").style.display="none"; 
    }else{   
      document.getElementById("selected").style.display="block";
    }
    
    
  };
  
 }catch(err){
   
 }
 finally{
  return this.getStates;
 }
 
}

render() {
  const modeltyp=(
    <form >      
      <label for= "modell" style={{ color:"#F0FFF0" ,fontSize:'20', display: "inline", width: "8em"}}><b>Type of model</b></label>
      <select id="modell" name="Modelllist" style={{margin:"10px"}}>
        <option value="" selected disabled hidden>Choose here</option>
        <option value="Stateflow">Stateflow</option>
        <option value="Simulink">Simulink</option> 
      </select> 
        <input type="button" id ="btn" className="btn btn-primary" value="Submit" />
    </form>
     
  );
  return (
    <div>
      <div id="selected" style={{padding:"10px", display :"block", border:" 2px solid #F0FFF0", textAlign: "center" , borderStyle: "dashed"}}> {modeltyp}</div>
     <div className='body' id="model" style={{ visibility:"hidden"}} > 
       <SVG src={mySvg} id="svg" onload={this.loadDivElemente()}/>
       <div className="context" hidden style={{ 
        boxSizing:' border-box', backgroundColor: '#FFFFFF',fontSize:'15px',zIndex:'80',
        left: '10', top: '10', color: '#000000', position: 'absolute', width: '200px', height: 'auto', padding: '5px 0px', cursor: "-webkit-grabbing", cursor: "grabbing"
      }}>
         <div style={{ position: "relative",zIndex:'80',display:'inline',fontSize:'15px',textAlign:'center'}}>
          <div onClick={this.setBreakpoint_on_modell} style={{ position: "relative",zIndex:'80'}} > set breakpoint 
          </div> 
          <div  onClick={this.removeBreakpoint_on_modell} > delete breakpoint </div>
        </div>
      </div>
       </div>
     </div>
  );
}
}

export default ModelDiag;