/**
 * A component to render source code, assembly, and break points
 */

import { store } from "statorgfc";
import React from "react";
import SVG from "react-inlinesvg";
import mySvg from "../../../tempfiles/f14.svg";
import React from 'react';
import GdbApi from './GdbApi.jsx';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

class ModelDiag extends React.Component {

  constructor(props) {
    super(props);
     
    this.states = ["MLLLL"];
    this.states1=["MCLCLCLCL"];
    this.svgElemente=[];
    this. arr = [];
    this.loadFunction = this.loadFunction.bind(this);
    this.addContextMenu = this.addContextMenu.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClick3 = this.handleClick3.bind(this);
    this.newStates = [];
    this.tempArray = [];
    this.labelstring = [];
    this.array = 0;
    this.map = [];
    this.statesName1 = [];
    this.breakpointId = 0;
    window.breakpointId = 0;
    this.resize = true;
    this.dateityp=0;   
    this.myFunction = this.myFunction.bind(this);
    this.checkCurrentState = this.checkCurrentState.bind(this);
    this.PrufAufDupliziertheit = this.PrufAufDupliziertheit.bind(this);

  }
  componentDidMount() {
    setInterval(() => this.myFunction(), 1000);
    setInterval(() => this.checkCurrentState(), 1000);
    this.array = this.handleClick2();

  }
/*Der XML DAtei beinhaltet mehrere Objekten und daraus sollten die Name der Zustände geparst und gespeichert werden. 
  eine Recursive Parse der Datei findet hier statt
  Hier den Parser wird gewählt je nach der Art der angegebenen Datei. 
  Alle Stateflow Datei haben als erster Parentelement: machine und unterscheidet sich somit von simulink Datei*/
  handleClick2= function () {
  // navigiere zuerst bis , Stateelemente( wo sich Zuständenamen befindet). 
     
    var data = require('../../../tempfiles/system_root_F14.xml');

    var firstParse = Object.values(data)[0];
    this.dateityp = Object.keys(firstParse)[0];      
// Also falls das gefundene Parentelement nicht machine ist, dann handelt sich um ein simulink Dateien            
      if ( Object.keys(firstParse)[0] != 'machine'){ 
// Für Simulink Datei.
    var secondParse = Object.values(firstParse)[1];
    console.log(secondParse);

    return secondParse;             
       
    } else{
// für Stateflow-Datei
    var secondParse = Object.values(firstParse)[0];
    var thirdParse = Object.values(secondParse)[0];
    var fourthParse = Object.values(thirdParse)[2];
    var fifthParse = Object.values(fourthParse)[0];
    var sixthParse = Object.values(fifthParse)[1];
    var seventhParse = Object.values(sixthParse)[0];
    var lastParse;

    for (var s = 0; s < Object.keys(seventhParse).length; s++) {

      if (Object.keys(seventhParse)[s] === "Children") {
        lastParse = Object.values(seventhParse)[s];
      }

    }

    return lastParse;

    }
  }
// duplizierete Elemente sollen raus dank dieser funktionen, weil manche Statenamen werden zweimal gefangen und gespeichert
  PrufAufDupliziertheit = function (Array) {

    let uniqueChars = Array.filter((c, index) => {
      return Array.indexOf(c) === index;
    });

    return uniqueChars;
  }
handleClick3= function (path) {
    var i=0;
    var tenParse = [];
    var neuState=[];
    var children = []; 
/* finde zuerst alle g Elemente der Zustände. Diese helfen dabei, alle state auf dem SVG DAtei zu identifizieren
 DABEI sollte man auch immer unterscheiden, falls es sich um Stateflow oder SImulink Datei handelt*/

if(this.dateityp==="machine"){
  for (i = 1; i < document.getElementsByTagName("g").length; i++) {
    if (document.getElementsByTagName("g")[i].innerHTML.replace(new RegExp("[^MLC]","g"),"")==this.states1[0] 
       ) {
      this.arr.push(document.getElementsByTagName("g")[i].getBoundingClientRect());
      console.log(document.getElementsByTagName("g")[i].getBBox());
      console.log(document.getElementsByTagName("g")[i])
    }
  }
// parse all State in XML Datei ( der Parser von Stateflow-DAteien)
    for (var s = 0; s < Object.keys(Object.values(path)[0]).length; s++) {
      if (Object.keys(Object.values(path)[0])[s] === "state") {
        tenParse = Object.values(Object.values(path)[0])[s];
      }
    }
    // prüfe der erste State Element, speiche die erste Labelstring der XML Datei im Array
    // falls er ein Kinder hat, dann speiche ihn in children
    for (var s = 0; s < Object.values(tenParse).length; s++) {
      var child = Object.values(Object.values(tenParse)[s]).length;
      if (Object.values(Object.values(Object.values(tenParse)[s])[1][5])[0] != "FUNC_STATE" &&
        Object.values(Object.values(Object.values(tenParse)[s])[1][3])[0] != 0
      ) {
        if (Object.values(Object.values(Object.values(tenParse)[s])[1][4])[0] === "OR_STATE" ||
          Object.values(Object.values(Object.values(tenParse)[s])[1][5])[0] === "OR_STATE"
        ) {
          this.labelstring.push(Object.values(Object.values(Object.values(tenParse)[s])[1][0]));
        }
      }

      // falls die gefundene State children haben dann parse sie 
      if (Object.keys(Object.values(Object.values(tenParse)[s])[child - 1][0])[0] === "state") {
        children = Object.values(Object.values(tenParse)[s])[child - 1][0];
        for (var l = 0; l < Object.keys(children).length; l++) {
          if (Object.keys(children)[l] === "state") {
            for (var m = 0; m < Object.values(children)[l].length; m++) {
              var child = Object.values(Object.values(children)[l][m]).length;
              neuState = Object.keys(Object.values(Object.values(children)[l][m])[child - 1][0]);
              if (Object.values(Object.values(Object.values(Object.values(children)[l][m])[1][5]))[0] != "FUNC_STATE" &&
                Object.values(Object.values(Object.values(Object.values(children)[l][m])[1][3]))[0] != 0
              ) {
                if (Object.values(Object.values(Object.values(Object.values(children)[l][m])[1][4]))[0] === "OR_STATE" ||
                  Object.values(Object.values(Object.values(Object.values(children)[l][m])[1][5]))[0] === "OR_STATE"
                ) {
                  this.labelstring.push(Object.values(Object.values(Object.values(children)[l][m])[1][0]));
                }
              }
              for (var i = 0; i < neuState.length; i++) {
                if (neuState[i] === "state") {
                  this.tempArray.push(Object.values(Object.values(children)[l][m])[child - 1][0]);
                  this.handleClick3(this.tempArray);
                }
              }
            }
          }
        }
      }
    }
// gefundene Statenamen sollen in Labelstring gespeichert werden    
return this.labelstring;
}else{

  for (i = 1; i < document.getElementsByTagName("g").length; i++) {
    if((document.getElementsByTagName("g")[i].innerHTML.replace( new RegExp("[^MLC]","g"),"")== this.states[0] &&
       document.getElementsByTagName("g")[i].innerHTML.split(" ")[1]=='d="M0,0' && 
       document.getElementsByTagName("g")[i].getAttribute("fill")=="none" 
    && document.getElementsByTagName("g")[i].getAttribute("stroke")!="none")|| 
    (document.getElementsByTagName("g")[i].innerHTML.replace( new RegExp("[^MLC]","g"),"")== this.states1[0]) 
    && document.getElementsByTagName("g")[i].getAttribute("fill")=="none" ){
      this.arr.push(document.getElementsByTagName("g")[i].getBoundingClientRect());
      console.log(document.getElementsByTagName("g")[i].getBoundingClientRect()); 
       
    } 
    
 }
// parse all State in XML Datei ( der Parser von Simulink-Dateien)
console.log(path);
for( var s=0; s<path.length ; s++){
    
     this.labelstring.push(Object.values(Object.values(Object.values(path))[s])[0].Name);
     console.log(Object.values(path)[s]);
     
     } 
     
   
 // gefundene Statenamen sollen in Labelstring gespeichert werden    
    return this.labelstring ; 
}

}
 
loadFunction() {
    var i = 0;
    let n = 0;
    var m =0;
    var statesName = [];

 /* handelclick3 liefert ein Objekt von allen Labelstring,aber alle sind nicht unbedingt statenamen 
   und es wird daraus die richtige State genommen und in map array gespeichert.*/
  statesName=this.handleClick3(this.array);     
      for( i=0; i<statesName.length;i++){
        if(this.dateityp!="machine"){ 
           if(statesName[i].includes("\n")){
              this.map.push(statesName[i].split("\n")[0].toLowerCase());
            }else { 
             this.map.push(statesName[i].toLowerCase());
            }
        }else if(this.dateityp==="machine"){
          if (statesName[i][0].includes("\n")) {
            this.map.push(statesName[i][0].split("\n")[0].toLowerCase());
          } else {
             this.map.push(statesName[i][0].toLowerCase());
          }
       }         
      }
  
  /*Zur Sicherheit dass die gefundenen State tatsächlich Statenamen sind:
      geht alle Text Elemente der SVG DAtei durch und prüfe 
     falls die gefundene Element sich in dem Array "map", dann nehme dieses Element.
      Map Array enthält Statesname, die nach dem Parsern gefunden wurde.*/
 // finde erstmal alle Zuständenamen in den Svgdatei.        
 for(m=0 ; m<document.getElementsByTagName("text").length ;m++){ 
   this.svgElemente.push(document.getElementsByTagName("text")[m].innerHTML.toLowerCase());
  
  }

 /* for(var item in this.svgElemente){ 
     if(this.map.includes(this.svgElemente[item])){
      this.statesName1.push(this.svgElemente[item]);    
     }
  
  }*/
  for(m=0 ; m<this.svgElemente.length ;m++){ 
    for(var item in this.map){    
    if(this.map[item].trim() === this.svgElemente[m].trim()){
        this.statesName1.push(this.svgElemente[m]);
          
    }
    }
  }
 
console.log(this.statesName1);
console.log(this.map);
// baut auf jede ZUstände ein unsichbares div, damit das setzen von Breakpoint visualisiert wird.

  for (n = 0; n <this.PrufAufDupliziertheit(this.statesName1).length; n++) { 

    this.k = document.createElement('div');
   
    document.getElementById('model_diag_view').appendChild(this.k);     
    this.k.id = this.PrufAufDupliziertheit(this.statesName1)[n];     
    this.k.setAttribute("style", "y:" + (this.arr[n].y) + "px; border-radius:6px;  x:" + (this.arr[n].x) + "px; rigth:" + (this.arr[n].right) + "px; left:" + (this.arr[n].left) + "px; bottom:" + (this.arr[n].bottom) + "px; width:" + (this.arr[n].width) + "px; height:" + (this.arr[n].height) + "px;top:" + (this.arr[n].top) + "px;position:absolute ;float:inherit;visibility: visible;");

    this.newStates[n] = this.k;    
    console.log(this.newStates[n]);            
  }
  this.addContextMenu();

}
// Contextmenu wird erstellt nach einem linken KLick auf den unsichbaren div(an der Stelle wo Zustände sich befindet)
addContextMenu() {
  var id = 0;
  this.newStates.forEach(function (elem) {
    $(elem).bind("contextmenu", function (event) {
      window.breakpointId = elem.id;

      event.preventDefault();
      $(".context")
        .show()
        .css({
          top: event.pageY + 15,
          left: event.pageX + 10,
          
        });
    });
  })

}
//der Div wird Rot zur Benachrichtigung, das ein Breakpoint gesetzt wurde  

handleClick() {

  this.newStates.forEach(function (elem) {

    if (window.breakpointId === elem.id) {
      GdbApi.model_breakpoint_set_on_state(elem.id);
      elem.style.border = " thick solid #de1b1f ";
      
    }
    $(".context").fadeOut("fast");
  });
}
myFunction() {
  this.newStates.forEach(function (elem) {
         if(GdbApi.Test()[0]!=null ){ 

             if (GdbApi.Test()[0].payload.split(" ")[3] === elem.id) {

                 if (GdbApi.Test()[1].payload.split(" ")[1] === "set"){ 
                    elem.style.border = " thick solid #337ab7 ";
        
                  }
                  if (GdbApi.Test()[1].payload.split(" ")[1] === "line\t"){ 
                     elem.style.border = "none";
          
              }
            }
  }

  $('#model_diag_view').click(function(){ 
    $(".context").fadeOut("fast");
});
  });

}

checkCurrentState() {
  this.newStates.forEach(function (elem) {
    if (store.get("currentState").trim() === elem.id.trim()) {
      elem.style.outline = " solid 6px #888 ";
    } else {
      elem.style.outline = " none ";
    }
  });
}
render() {

  return (
    <div className='header'>
      <SVG src={mySvg} onLoad={this.loadFunction} />

      <div className="context" hidden style={{ 
        boxSizing:' border-box', backgroundColor: '#FFFFFF',fontSize:'15px',zIndex:'80',
        left: '10', top: '10', color: '#000000', position: 'absolute', width: '200px', height: 'auto', padding: '5px 0px', cursor: "-webkit-grabbing", cursor: "grabbing"
      }}>
         <div style={{ position: "relative",zIndex:'80',display:'inline',fontSize:'15px',textAlign:'center'}}>
          <div onClick={this.handleClick} style={{ position: "relative",zIndex:'80'}} >breakpoint setzen
          </div> 
          <div  onClick={this.handleClick2} >breakpoint aufnehmen
          </div>
        </div>
      </div>
    </div>
  );
}
}
export default ModelDiag;
