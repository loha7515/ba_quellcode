/**
 * A component to render source code, assembly, and break points
 */

import { store } from "statorgfc";
import React from "react";
import SVG from "react-inlinesvg";
import mySvg from "../../../tempfiles/sf_boiler_Bang-Bang_Controller1.svg";      
import React from 'react';
import GdbApi from './GdbApi.jsx';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

class ModelDiag extends React.Component {

  constructor(props) {       
    super(props);
    this.loadFunction = this.loadFunction.bind(this);
    this.addContextMenu = this.addContextMenu.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClick3 = this.handleClick3.bind(this);
    this.newStates = [];
    this.tempArray=[];
    this. labelstring=[];
    this.array = 0;
    this.map = [];
    this.statesName1=[];
    this.breakpointId = 0;
    window.breakpointId = 0;
    this.resize = true;
    this.myFunction = this.myFunction.bind(this);
    this.checkCurrentState = this.checkCurrentState.bind(this); 
    this.PrufAufDupliziertheit=this.PrufAufDupliziertheit.bind(this); 
             
  }
  componentDidMount() {
    setInterval(() => this.myFunction(), 1000);
    setInterval(() => this.checkCurrentState(), 1000);
    this.array=this.handleClick2();
   
  }
    // der XML DAtei beinhaltet mehrere Objekten , damit die Name der ZUstand vergleichen werden sollen alle diese
  // Objeken in FOrm von String in einem Array gepeichert werden , eine Recursive Parse der DAtei findet hier statt
  handleClick2= function () {
    // navigiere zuerst bis , Stateelemente. 
     
    var data = require('../../../tempfiles/blockdiagram.xml');

    var firstParse = Object.values(data)[0];
    console.log(firstParse);
    var secondParse = Object.values(firstParse)[0];
    console.log(secondParse);   
    var thirdParse = Object.values(secondParse)[0];
    var fourthParse = Object.values(thirdParse)[2];
    var fifthParse = Object.values(fourthParse)[0];
    var sixthParse = Object.values(fifthParse)[1];
    var seventhParse = Object.values(sixthParse)[0];
    var lastParse ;
    for( var s=0; s<Object.keys(seventhParse).length ; s++){

      if(Object.keys(seventhParse)[s]==="Children"){
        lastParse = Object.values(seventhParse)[s];
      }

    }
    return lastParse;             

  }
  // duplizierete Elemente sollen raus
PrufAufDupliziertheit = function(Array){
  
  let uniqueChars = Array.filter((c, index) => {
       return Array.indexOf(c) === index;
   });
 
   return uniqueChars ;     
  }
  
  
handleClick3= function (path) {
   // Ab dem elfParse sind Stateelemente gelistet
    var tenParse = [];
    var neuState=[];
    var children = []; 
// parse all State in XML Datei und speichere es in tenparse
//while(bool){
  for( var s=0; s<Object.keys(Object.values(path)[0]).length ; s++){
      if(Object.keys(Object.values(path)[0])[s]==="state"){
         tenParse = Object.values(Object.values(path)[0])[s];
      }
    } 
     // prüfe der erste State Element, speiche die erste Labelstring der XML Datei im Array
    // falls er ein Kinder hat, dann speichere ihn in children
    for( var s=0; s<Object.values(tenParse).length ; s++){
      var child = Object.values(Object.values(tenParse)[s]).length; 
      if(Object.values(Object.values(Object.values(tenParse)[s])[1][5])[0]!="FUNC_STATE" &&
         Object.values(Object.values(Object.values(tenParse)[s])[1][3])[0]!=0
        )
      {  
        if(Object.values(Object.values(Object.values(tenParse)[s])[1][4])[0]==="OR_STATE" ||
          Object.values(Object.values(Object.values(tenParse)[s])[1][5])[0]==="OR_STATE"
        ){  
           this. labelstring.push(Object.values(Object.values(Object.values(tenParse)[s])[1][0]));
         }
      }
    
      // falls die gefundene State children haben dann parse sie 
            if(Object.keys(Object.values(Object.values(tenParse)[s])[child-1][0])[0]==="state"){
                children = Object.values(Object.values(tenParse)[s])[child-1][0];
                for( var l=0; l<Object.keys(children).length ; l++){
                  if(Object.keys(children)[l]==="state"){
                    for( var m=0; m<Object.values(children)[l].length ; m++){ 
                    var child = Object.values(Object.values(children)[l][m]).length;
                    neuState  = Object.keys(Object.values(Object.values(children)[l][m])[child-1][0]);
                    if(Object.values(Object.values(Object.values(Object.values(children)[l][m])[1][5]))[0]!="FUNC_STATE"&&
                       Object.values(Object.values(Object.values(Object.values(children)[l][m])[1][3]))[0]!=0
                      )
                    {
                      if(Object.values(Object.values(Object.values(Object.values(children)[l][m])[1][4]))[0]==="OR_STATE" ||
                         Object.values(Object.values(Object.values(Object.values(children)[l][m])[1][5]))[0]==="OR_STATE"
                         ){  
                         this. labelstring.push(Object.values(Object.values(Object.values(children)[l][m])[1][0]));
                         }
                    }
                    for(var i=0; i<neuState.length; i++){
                          if(neuState[i]==="state"){
                            this.tempArray.push(Object.values(Object.values(children)[l][m])[child-1][0]);
                            this.handleClick3(this.tempArray); 
                          }
                        }
                   }
                  }
                 }
            }
    }
    
    return this.labelstring ; 
 
}

  
  loadFunction() {
    var i = 0;
    var j = 0;
    let n = 0;
    var m =0;
    var z =0; 
    var states = ["MCLCLCLCL"];
    var arr = [];
    var gElementsForStates = [];
    var statesName = [];

  // handelclick3 liefert ein Objekt von allen Labelstring und es wird die richtige State genommen.
  //wir kennen alle Methode von Mathelab. falls ein Labelstring aus dem Parser unterschiedlich ist als diesen Methoden, dann sind diese States
  statesName=this.handleClick3(this.array);     
  console.log(statesName);                                                            
  for( i=0; i<statesName.length;i++){

            if(statesName[i][0].includes("\n")){
              this.map.push(statesName[i][0].split("\n")[0]);
             }else { 
              this.map.push(statesName[i][0]);
             }
                   
  }


    for (i = 1; i < document.getElementsByTagName("g").length; i++) {
      if (document.getElementsByTagName("g")[i].innerHTML.replace(new RegExp("[^MLC]", "g"), "") == states[0] 
         ) {
        gElementsForStates[j]  = document.getElementsByTagName("g")[i];
        arr[j] = document.getElementsByTagName("g")[i].getBoundingClientRect();
        
        j++;  
      }
      
   }
   console.log(this.PrufAufDupliziertheit(arr));   
    
    /* geht alle Text Elemente der SVG DAtei durch und prüfe 
       falls die gefundene Element sich in dem Array "map" befindet, dann nehme dieses Element.
        Map Array enthält Statesname, die nach dem Parsern gefunden wurde.*/
            
    for(m=0 ; m<document.getElementsByTagName("text").length ;m++){    
      for(var item in this.map){    
      if(this.map[item] === document.getElementsByTagName("text")[m].innerHTML){
          this.statesName1.push(document.getElementsByTagName("text")[m].innerHTML);
            
      }
      }
    }
  
 console.log(this.PrufAufDupliziertheit(this.statesName1));
    for (n = 0; n <this.PrufAufDupliziertheit(this.statesName1).length; n++) { 

      this.k = document.createElement('div');
     
      document.getElementById('model_diag_view').appendChild(this.k);     
      this.k.id = this.PrufAufDupliziertheit(this.statesName1)[n];      
      this.k.setAttribute("style", "y:" + (arr[n].y) + "px; border-radius:6px;  x:" + (arr[n].x) + "px; rigth:" + (arr[n].right) + "px; left:" + (arr[n].left) + "px; bottom:" + (arr[n].bottom) + "px; width:" + (arr[n].width) + "px; height:" + (arr[n].height) + "px;top:" + (arr[n].top) + "px;position:absolute ;float:inherit;visibility: visible;");

      this.newStates[n] = this.k;  
      console.log(this.newStates[n]);                
    }
    this.addContextMenu();

  }
  addContextMenu() {
    var id = 0;
    this.newStates.forEach(function (elem) {
      $(elem).bind("contextmenu", function (event) {
        window.breakpointId = elem.id;

        event.preventDefault();
        $(".context")
          .show()
          .css({
            top: event.pageY + 15,
            left: event.pageX + 10,
            
          });
      });
    })

  }
  handleClick() {

    this.newStates.forEach(function (elem) {

      if (window.breakpointId === elem.id) {
        GdbApi.model_breakpoint_set_on_state(elem.id);
        elem.style.border = " thick solid #de1b1f ";
        
      }
      $(".context").fadeOut("fast");
    });
  }
  
   
myFunction() {
    this.newStates.forEach(function (elem) {
           if(GdbApi.Test()[0]!=null ){ 

               if (GdbApi.Test()[0].payload.split(" ")[3] === elem.id) {

                   if (GdbApi.Test()[1].payload.split(" ")[1] === "set"){ 
                      elem.style.border = " thick solid #337ab7 ";
          
                    }
                    if (GdbApi.Test()[1].payload.split(" ")[1] === "line\t"){ 
                       elem.style.border = "none";
            
                }
              }
    }

    $('#model_diag_view').click(function(){ 
      $(".context").fadeOut("fast");
  });
    });
 
}

  checkCurrentState() {
    this.newStates.forEach(function (elem) {
      if (store.get("currentState").trim() === elem.id.trim()) {
        elem.style.outline = " solid 6px #888 ";
      } else {
        elem.style.outline = " none ";
      }
    });
  }



  render() {

    return (
      <div className='header'>
        <SVG src={mySvg} onLoad={this.loadFunction} />

        <div className="context" hidden style={{ 
          boxSizing:' border-box', backgroundColor: '#FFFFFF',fontSize:'15px',zIndex:'80',
          left: '10', top: '10', color: '#000000', position: 'absolute', width: '200px', height: 'auto', padding: '5px 0px', cursor: "-webkit-grabbing", cursor: "grabbing"
        }}>
           <div style={{ position: "relative",zIndex:'80',display:'inline',fontSize:'15px',textAlign:'center'}}>
            <div onClick={this.handleClick} style={{ position: "relative",zIndex:'80'}} >breakpoint setzen
            </div> 
            <div  onClick={this.handleClick2} >breakpoint aufnehmen
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ModelDiag;